
#include "MultiscaleEntropy.h"
#include "EigenLinalgIO.h"

#include <tclap/CmdLine.h>

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Multiscale Entropy", ' ', "1");

  TCLAP::ValueArg<std::string> xArg("x","X", "Matrix to compute entropy for, each row is a time series", true, "",
      "matrix header file");
  cmd.add(xArg);
  
  
  TCLAP::ValueArg<int> mArg("m","segment", "Length of segment at each scale", false, 2, "integer");
  cmd.add(mArg);
  
  TCLAP::ValueArg<int> sArg("s","scale", "Maximum scale to compute entropy for", false, 20, "integer");
  cmd.add(sArg);
  
  TCLAP::ValueArg<double> rArg("r","radius", "Radius for entropy computation as a factor of standard deviation of the time series in each row", false, 0.15, "double");
  cmd.add(rArg);

  try{ 
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

  int maxScale = sArg.getValue();
  int m = mArg.getValue();
  double r = rArg.getValue();
  Eigen::MatrixXd X = EigenLinalg::LinalgIO<double>::readMatrix( xArg.getValue() );

  //Not used currently
  Eigen::VectorXd time;
  MultiscaleEntropy<double> entropy(X, time);
  Eigen::MatrixXd ent = entropy.computeEntropy(maxScale, m, r);

  
  
  EigenLinalg::LinalgIO<double>::writeMatrix("entropy.data", ent );
  
  return 0;
}
