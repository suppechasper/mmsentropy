\name{multiscale.entropy}
\alias{multiscale.entropy}
\title{Multiscale Entropy}
\description{


}
\usage{
multiscale.entropy(x, delay, scale, r, fixedRadius=TRUE)
}
\arguments{
  \item{x}{matrix with each column measurements of a time series}
  \item{delay}{Delay embedding size}
  \item{scale}{Number of scales to compute entropy for.}
  \item{r}{Radius eitehr percentage or fixed (see below)}
  \item{fixed}{If true (defualt) use fixed radius r otherwise r is percentage of
  stdev in each channel and trace for multivariate analysis}
}
\value{
 A matrix with each row the multiscale entropy for the corresponding time series
   in the columns of x. The last row is the joint entropy for the multivariate
   timeseries.
}
\author{
  Samuel Gerber
}
\examples{

  library(tuneR)

  w1 <- noise(kind = c("white"), samp.rate=10000)
  #w2 <- noise(kind = c("white"), samp.rate=10000)
  #w3 <- noise(kind = c("white"), samp.rate=10000)
  p1 <- noise(kind = c("pink"), samp.rate=10000)
  p2 <- noise(kind = c("pink"), samp.rate=10000)
  p3 <- noise(kind = c("pink"), samp.rate=10000)

  X = cbind(w1@left, p1@left, p2@left, p3@left)
  ent = multiscale.entropy(X, 2, 20, 0.25)

  plot(ent[5, ], t="l", ylim=range(ent))
  lines(ent[1, ], col="blue")
  lines(ent[2, ], col="red")
  lines(ent[3, ], col="red")
  lines(ent[4, ], col="red")

}
\keyword{multiscale entropy}
