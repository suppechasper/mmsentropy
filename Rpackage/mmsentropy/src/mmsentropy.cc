#ifndef NULL
#define NULL 0
#endif

#define R_NO_REMAP
#include <R.h>
#include <Rinternals.h>
//#include <Rdefines.h>


#include "MultiscaleEntropy.h"

extern "C" {


SEXP multiscaleEntropy(SEXP Rm, SEXP Rn, SEXP Rx, 
                       SEXP Rdelay, SEXP Rscale, SEXP Rr, SEXP Rfixed) {

  
  double *x = REAL(Rx);
  int m = *INTEGER(Rm);
  int n = *INTEGER(Rn);
  
  int delay = *INTEGER(Rdelay);
  int scale = *INTEGER(Rscale);
  double r = *REAL(Rr);
  bool fixed = *INTEGER(Rfixed) != 0;
 
  Eigen::MatrixXd X = Eigen::Map<Eigen::MatrixXd>(x,m,n);
  //unequal sampling times not implemented at the moment
  Eigen::VectorXd time;

  MultiscaleEntropy<double> mse(X, time);

  Eigen::MatrixXd ent = mse.computeEntropy(scale, delay, r, fixed);
  
  SEXP Rent;
  PROTECT( Rent = Rf_allocMatrix(REALSXP, ent.rows(), ent.cols()));
  memcpy( REAL(Rent), ent.data(), ent.rows()*ent.cols()*sizeof(double) );
  UNPROTECT( 1 );

  return Rent;  

};




}//end extern C
