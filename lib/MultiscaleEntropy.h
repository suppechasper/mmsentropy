
#ifndef MULTISCALEENTROPY_H
#define MULTISCALEENTROPY_H

#include "Eigen/Dense"
#include <vector>

#include <cmath>
#include <limits>


#include <iostream>

template <typename TPrecision>
class MultiscaleEntropy{



  public:
    
    typedef typename Eigen::Matrix<TPrecision, Eigen::Dynamic, 1> VectorXp;
    typedef typename Eigen::Matrix<TPrecision, Eigen::Dynamic, Eigen::Dynamic> MatrixXp;
  
  

  private:
  
    const Eigen::Ref<const MatrixXp> X;
    const Eigen::Ref<const VectorXp> time;
    VectorXp stdev;
    VectorXp radius;
    TPrecision trace;
    TPrecision tRadius;



  public:
    
    MultiscaleEntropy(const Eigen::Ref<const MatrixXp> r_X, const Eigen::Ref<const VectorXp> r_time) : 
      X(r_X), time(r_time){
       VectorXp means = X.rowwise().sum() / X.cols();
       stdev = ( (X.colwise() - means).rowwise().squaredNorm() / (X.cols() - 1) );
       trace = sqrt( stdev.sum() );
       stdev = stdev.array().sqrt();

    };

    

    MatrixXp computeEntropy(int maxScale, int m, TPrecision r, bool fixedRadius = true){
      if(fixedRadius){
        radius = VectorXp::Constant(X.rows(), r);
        tRadius = sqrt( X.rows() ) * r;
      }
      else{
        radius = stdev * r;
        tRadius = trace * r;
      } 
       


      std::cout << radius  << std::endl;
      MatrixXp ent = MatrixXp::Zero(X.rows()+1, maxScale); 
      for(int i=1; i<=maxScale; i++){
        ent.col(i-1) = sampleEntropy(i, m);
      }

      return ent;
    }; 
   



  private:
    
    VectorXp sampleEntropy(int scaling, int m){
      

      //Coarsening
      MatrixXp Xc;
      if(scaling == 1 ){
        Xc = X;
      }
      else{
        int n = floor( (X.cols()-scaling) / (double) scaling );
        Xc = MatrixXp(X.rows(), n);
        for(int i=0; i<Xc.cols(); i++){
          int ii = i*scaling;
          Xc.col(i) = X.block(0, ii, X.rows(), scaling).rowwise().sum() / scaling;
        }
      }


      VectorXp C1 = VectorXp::Zero( X.rows()+1 );
      VectorXp C2 = VectorXp::Zero( X.rows()+1 );
      

      MatrixXp D1( Xc.rows(), Xc.cols()-m );
      
      for(int i=0; i < D1.cols(); i++){
        D1.setZero();
        for(int j=0;j<m; j++){
          MatrixXp d = (Xc.colwise() - Xc.col(i+j)).array().abs();
          for(int ii=0; ii<D1.cols(); ii++){
            for(int jj=0; jj<D1.rows(); jj++){
              if( D1(jj, ii) < d(jj, ii+j) ){
                D1(jj, ii) = d(jj, ii+j);
              }
            }
          }
        }

        MatrixXp D2 = D1;
        MatrixXp d = (Xc.colwise() - Xc.col(i+m)).array().abs();
        for(int ii=0; ii<D2.cols(); ii++){
          for(int jj=0; jj<D2.rows(); jj++){
            if( D2(jj, ii) < d(jj, ii+m) ){
              D2(jj, ii) = d(jj, ii+m);
            }
          }
        }


        for(int ii=0; ii< D1.cols(); ii++){
          if(ii == i){ 
            continue;
          }

          int nT1 = 0;
          int nT2 = 0;
          for(int jj=0; jj<D1.rows(); jj++){


            if(D1(jj, ii) < radius(jj) ){
              nT1++;
              C1(jj) += 1;
            }
            //else if( D1(jj, ii) < tRadius ){
            // nT1++;
            //}
            if(D2(jj, ii) < radius(jj) ){
              nT2++;
              C2(jj) += 1;
            }
            //else if( D2(jj, ii) < tRadius ){
            //  nT2++;
            //}
          }
          if( nT1 == D1.rows() ){
            C1( D1.rows() ) += 1;
            if( nT2 == D2.rows() ){
              C2( D2.rows() ) += 1;
            }
          }
        }


      }
      //C2( X.rows() ) /= X.rows();
      
      return ( C1.array() /  C2.array()  ).log();


    };

 
};
#endif
